package ru.t1.nlevchenko.tm.constant;

public class ArgumentConst {

    public static final String INFO = "-i";

    public static final String VERSION = "-v";

    public static final String HELP = "-h";

}
